var game = new Phaser.Game(800,850,Phaser.AUTO,'phaser',{
    preload: preload,
    create: create,
    update: update,
    render: render
});
var style = { font: "48px Arial", fill: "#ff0044", align: "center", backgroundColor: "#ffffff" };
var gameBoard
var playerPlacingUI
var player_money=0.0

objectTypes = Object.freeze({
    EMPTY: 1,
    ARROW: 2
})
objectDirections = Object.freeze({
    DIRECTION_UP: 1,
    DIRECTION_DOWN: 2,
    DIRECTION_LEFT: 3,
    DIRECTION_RIGHT: 4,
})
objectRoles = Object.freeze({
    ROLE_NONE:1,
    ROLE_BELT:2,
    ROLE_FACTORY:3,
    ROLE_STORAGE:4,
    ROLE_MINE:5,
    ROLE_EXPORT:6
})
resouceTypes = Object.freeze({
    RESOURCE_NONE:1,
    RESOURCE_COAL:2,
    RESOURCE_IRON:3
})

itemTypes = Object.freeze({
    ITEM_NONE:1,
    ITEM_COAL:2,
    ITEM_IRON_ORE:3,
    ITEM_IRON_BAR: 4
})

itemPrices=[]
itemPrices[itemTypes.ITEM_NONE]=0.0
itemPrices[itemTypes.ITEM_COAL]=0.05
itemPrices[itemTypes.ITEM_IRON_ORE]=0.05
itemPrices[itemTypes.ITEM_IRON_BAR]=0.2
Object.freeze(itemPrices)

function getDxDyFromDirection(Direction){
    if (Direction==objectDirections.DIRECTION_UP){
        return [0,-1]
    }
    if (Direction==objectDirections.DIRECTION_DOWN){
        return [0,1]
    }
    if (Direction==objectDirections.DIRECTION_LEFT){
        return [-1,0]
    }
    if (Direction==objectDirections.DIRECTION_RIGHT){
        return [1,0]
    }
    return undefined
}
function getOppositeDirection(Direction){
    if (Direction==objectDirections.DIRECTION_UP){
        return objectDirections.DIRECTION_DOWN
    }
    if (Direction==objectDirections.DIRECTION_DOWN){
        return objectDirections.DIRECTION_UP
    }
    if (Direction==objectDirections.DIRECTION_LEFT){
        return objectDirections.DIRECTION_RIGHT
    }
    if (Direction==objectDirections.DIRECTION_RIGHT){
        return objectDirections.DIRECTION_LEFT
    } 
    return undefined
}

function GameBoard() {
    this.boardSize = [50, 50]
    this.tileSize = 15
    this.gfx = game.add.graphics()
    this.gfx.lineStyle(1, 0x000000, 1);
    for (var i = 0; i < this.boardSize[0] + 1; i++) {
        this.gfx.moveTo(i * this.tileSize, 0);
        this.gfx.lineTo(i * this.tileSize, this.boardSize[1] * this.tileSize);
    }
    for (var i = 0; i < this.boardSize[1] + 1; i++) {
        this.gfx.moveTo(0, i * this.tileSize);
        this.gfx.lineTo(this.boardSize[0] * this.tileSize, i * this.tileSize);
    }
    backgroundGroup.add(this.gfx)
    this.locked=false
    this.factoryMap = []
    this.factorySpritesMap = []
    this.resourceMap = []
    this.resourceSpritesMap = []
    for (var i = 0; i < this.boardSize[0]; i++) {
        this.factoryMap[i] = []
        this.factorySpritesMap[i] = []
        this.resourceMap[i] = []
        this.resourceSpritesMap[i] = []
        for (var j = 0; j < this.boardSize[1]; j++) {
            this.factoryMap[i][j] = {
                Type: objectTypes.EMPTY,
                CurrentInventory:[0,0,0,0,0],
                NextInventory:[0,0,0,0,0],
                ItemType:[itemTypes.ITEM_NONE,
                        itemTypes.ITEM_NONE, itemTypes.ITEM_NONE,
                        itemTypes.ITEM_NONE,itemTypes.ITEM_NONE],
                Role: objectRoles.ROLE_NONE,
                Location:[i,j]
            }
            this.factoryMap[i][j].RunInternal=this.getInternalRunFunction(this.factoryMap[i][j].Role)
            this.factoryMap[i][j].RunExternal=this.getExternalRunFunction(this.factoryMap[i][j].Role)
            this.factorySpritesMap[i][j] = undefined
            var rnd=Math.random()*100
            if (rnd<1){
                this.resourceMap[i][j] = resouceTypes.RESOURCE_COAL
            }else if(rnd<2){
                this.resourceMap[i][j] = resouceTypes.RESOURCE_IRON
            }else{
                this.resourceMap[i][j] = resouceTypes.RESOURCE_NONE 
            }
            if (this.resourceMap[i][j]!=resouceTypes.RESOURCE_NONE){
                ret=this.getXYFromTile(i,j)
                if (this.resourceMap[i][j] == resouceTypes.RESOURCE_IRON){
                    this.resourceSpritesMap[i][j]=game.add.sprite(ret[0],ret[1], 'iron');
                }
                if (this.resourceMap[i][j] == resouceTypes.RESOURCE_COAL){
                    this.resourceSpritesMap[i][j]=game.add.sprite(ret[0], ret[1], 'coal');
                }
                this.resourceSpritesMap[i][j].anchor.setTo(0.5,0.5)
                var scale=this.tileSize/this.resourceSpritesMap[i][j].width
                this.resourceSpritesMap[i][j].scale.setTo(scale,scale)
                resourceGroup.add(this.resourceSpritesMap[i][j])
            }
        }
    }
}

GameBoard.prototype.getTileFromXY = function(x, y) {
    if (x >= this.tileSize * this.boardSize[0]) {
        return undefined;
    }
    if (y >= this.tileSize * this.boardSize[1]) {
        return undefined;
    }
    return [Math.floor(x / this.tileSize), Math.floor(y / this.tileSize)]
}

GameBoard.prototype.getXYFromTile = function(x, y) {
    return [x * this.tileSize + this.tileSize / 2, y * this.tileSize + this.tileSize / 2]
}

GameBoard.prototype.isTileFree = function(x, y) {
    var obj=this.getTileObj(x,y)
    if (obj){
        return obj.Type == objectTypes.EMPTY
    }
    return false;
}

GameBoard.prototype.getTileObj = function(x, y) {
    if ((x >= this.boardSize[0]) || (y >= this.boardSize[1])) {
        return undefined;
    }
    if ((x <0) || (y <0)) {
        return undefined;
    }
    return this.factoryMap[x][y]
}

GameBoard.prototype.setTile = function(x, y, obj) {
    if ((x >= this.boardSize[0]) || (y >= this.boardSize[1])) {
        return
    }
    this.factoryMap[x][y].Type = obj.Type
    this.factoryMap[x][y].Direction = obj.Direction
    this.factoryMap[x][y].Role=this.determineRole(x,y)
    this.factoryMap[x][y].RunInternal=this.getInternalRunFunction(this.factoryMap[x][y].Role)
    this.factoryMap[x][y].RunExternal=this.getExternalRunFunction(this.factoryMap[x][y].Role)

    for (var key in objectDirections){
        var dxdy=getDxDyFromDirection(objectDirections[key])
        var neighbour=this.getTileObj(x+dxdy[0],y+dxdy[1])
        if (neighbour){
            neighbour.Role=this.determineRole(x+dxdy[0],y+dxdy[1])
            neighbour.RunInternal=this.getInternalRunFunction(neighbour.Role)
            neighbour.RunExternal=this.getExternalRunFunction(neighbour.Role)
            this.updateSprite(x+dxdy[0],y+dxdy[1]);
        }
    }
    this.updateSprite(x,y);
}

GameBoard.prototype.getInternalRunFunction=function(role){
    var gb=this;
    if (role==objectRoles.ROLE_BELT){
        return function(){
            if (this.CurrentInventory[this.Direction]==0){//according to belt direction
                if (this.CurrentInventory[getOppositeDirection(this.Direction)]!=0){
                    this.CurrentInventory[this.Direction]=this.CurrentInventory[getOppositeDirection(this.Direction)];
                    this.ItemType[this.Direction]=this.ItemType[getOppositeDirection(this.Direction)]
                    this.CurrentInventory[getOppositeDirection(this.Direction)]=0;
                }else{//check other input directions as well
                    for (var key in objectDirections){
                        if (objectDirections[key]==this.Direction) continue;
                        if (this.CurrentInventory[objectDirections[key]]!=0){
                            this.CurrentInventory[this.Direction]=this.CurrentInventory[objectDirections[key]];
                            this.ItemType[this.Direction]=this.ItemType[objectDirections[key]]
                            this.CurrentInventory[objectDirections[key]]=0;
                        }
                    }
                }
            }
        }
    }
    if (role==objectRoles.ROLE_FACTORY){
        return function(){
            coalAt=-1;
            oreAt=-1;
            for (var key in objectDirections){
                if (objectDirections[key]==this.Direction) continue;
                if (this.CurrentInventory[objectDirections[key]]!=0){
                    if (this.ItemType[objectDirections[key]]==itemTypes.ITEM_COAL){
                        coalAt=objectDirections[key];
                    }
                    if (this.ItemType[objectDirections[key]]==itemTypes.ITEM_IRON_ORE){
                        oreAt=objectDirections[key];
                    }
                }
            }
            if (coalAt!=-1 && oreAt!=-1 && this.CurrentInventory[this.Direction]==0){
                this.CurrentInventory[this.Direction]=1
                this.ItemType[this.Direction]=itemTypes.ITEM_IRON_BAR
                this.CurrentInventory[coalAt]=this.CurrentInventory[coalAt]-1;
                this.CurrentInventory[oreAt]=this.CurrentInventory[oreAt]-1;
            }
        }
    }
    if (role==objectRoles.ROLE_STORAGE){
        return function(){

        }
    }

    if (role==objectRoles.ROLE_EXPORT){
        return function(){
            for (var i=0;i<this.CurrentInventory.length;i++){
                player_money=player_money+itemPrices[this.ItemType[i]]*this.CurrentInventory[i];
                this.CurrentInventory[i]=0;
            }
        }
    }
    if (role==objectRoles.ROLE_MINE){
        return function(){
            var itemType=itemTypes.ITEM_NONE
            if (gb.resourceMap[this.Location[0]][this.Location[1]]==resouceTypes.RESOURCE_COAL){
                itemType=itemTypes.ITEM_COAL
            }
            if (gb.resourceMap[this.Location[0]][this.Location[1]]==resouceTypes.RESOURCE_IRON){
                itemType=itemTypes.ITEM_IRON_ORE
            }
            if (itemType!=itemTypes.ITEM_NONE){

               for (var key in objectDirections){
                   if (this.CurrentInventory[objectDirections[key]]==0){
                        this.CurrentInventory[objectDirections[key]]=1
                        this.ItemType[objectDirections[key]]=itemType
                   }
               }
           } 
        }
    }
    return function(){}
}

GameBoard.prototype.getExternalRunFunction=function(role){
    var gb=this;
    if (role==objectRoles.ROLE_MINE){
        return function(){
            for (var key in objectDirections){
                var dxdy=getDxDyFromDirection(objectDirections[key])
                var neighbour=gb.getTileObj(this.Location[0]+dxdy[0],this.Location[1]+dxdy[1])
                var oppDir=getOppositeDirection(objectDirections[key])
                if (neighbour && neighbour.Type!=objectTypes.EMPTY  && neighbour.CurrentInventory[oppDir]==0 ){//can transfer
                    if (neighbour.NextInventory[oppDir]!=0) {console.log('assert Next inv');}
                    neighbour.NextInventory[oppDir]=neighbour.NextInventory[oppDir]+this.CurrentInventory[objectDirections[key]];
                    neighbour.ItemType[oppDir]=this.ItemType[objectDirections[key]]
                }else{//keep current
                    this.NextInventory[objectDirections[key]]=
                        this.NextInventory[objectDirections[key]]+
                        this.CurrentInventory[objectDirections[key]];
                }
            }
        }
    }
    if (role==objectRoles.ROLE_BELT || role==objectRoles.ROLE_FACTORY){
        return function(){
            var dxdy=getDxDyFromDirection(this.Direction)
            var neighbour=gb.getTileObj(this.Location[0]+dxdy[0],this.Location[1]+dxdy[1])
            var oppDir=getOppositeDirection(this.Direction)
            //only for this.Direction
            if ((this.CurrentInventory[this.Direction]>0) && 
                neighbour && neighbour.Type!=objectTypes.EMPTY && 
                neighbour.CurrentInventory[oppDir]==0 ){//can transfer
                    if (neighbour.NextInventory[oppDir]!=0) {alert('assert Next inv');}
                    neighbour.NextInventory[oppDir]=neighbour.NextInventory[oppDir]+this.CurrentInventory[this.Direction];
                    neighbour.ItemType[oppDir]=this.ItemType[this.Direction]
            }else{//keep current
                this.NextInventory[this.Direction]=
                    this.NextInventory[this.Direction]+
                    this.CurrentInventory[this.Direction];
            }
            //for other directions
            for (var key in objectDirections){
                if (objectDirections[key]==this.Direction) continue;
                this.NextInventory[objectDirections[key]]=
                    this.NextInventory[objectDirections[key]]+
                    this.CurrentInventory[objectDirections[key]];
            }
        }
    }
    return function(){//do nothing - storage
        for (var key in objectDirections){
            this.NextInventory[objectDirections[key]]=
                this.NextInventory[objectDirections[key]]+
                this.CurrentInventory[objectDirections[key]];
        }
    }
}

GameBoard.prototype.updateSprite = function(x, y) {
    if (this.factorySpritesMap[x][y]) {
        this.factorySpritesMap[x][y].destroy()
    }
    var obj=this.factoryMap[x][y];
    if (obj.Type == objectTypes.EMPTY) {
        this.factorySpritesMap[x][y] = undefined
    }else{
         ret = this.getXYFromTile(x, y)
         if (obj.Role==objectRoles.ROLE_BELT){
            this.factorySpritesMap[x][y] = game.add.sprite(ret[0], ret[1], 'arrow');
        }else if (obj.Role==objectRoles.ROLE_STORAGE){
            this.factorySpritesMap[x][y] = game.add.sprite(ret[0], ret[1], 'Storage');
        }else if (obj.Role==objectRoles.ROLE_FACTORY){
            this.factorySpritesMap[x][y] = game.add.sprite(ret[0], ret[1], 'Factory');
        }else if (obj.Role==objectRoles.ROLE_MINE){
            this.factorySpritesMap[x][y] = game.add.sprite(ret[0], ret[1], 'Mine');
        }else if (obj.Role==objectRoles.ROLE_EXPORT){
            this.factorySpritesMap[x][y] = game.add.sprite(ret[0], ret[1], 'Export');
        }else{
            this.factorySpritesMap[x][y] = game.add.sprite(ret[0], ret[1], 'Unknown');
        }
        this.factorySpritesMap[x][y].anchor.setTo(0.5, 0.5)
        if (obj.Direction == objectDirections.DIRECTION_UP) {
            this.factorySpritesMap[x][y].angle = 0
        } else if (obj.Direction == objectDirections.DIRECTION_DOWN) {
            this.factorySpritesMap[x][y].angle = 180
        } else if (obj.Direction == objectDirections.DIRECTION_LEFT) {
            this.factorySpritesMap[x][y].angle = 270
        } else if (obj.Direction == objectDirections.DIRECTION_RIGHT) {
            this.factorySpritesMap[x][y].angle = 90
        }
        var scale=this.tileSize/this.factorySpritesMap[x][y].width
        this.factorySpritesMap[x][y].scale.setTo(scale,scale)
        factoryGroup.add(this.factorySpritesMap[x][y])
    }
}


GameBoard.prototype.determineRole=function(x,y){
    var self=this.getTileObj(x,y)
    if (!self || self.Type==objectTypes.EMPTY){
        return objectRoles.ROLE_NONE
    }
    var inputs=0
    var outputs=0
    var nextToResource=false;
    for (var key in objectDirections){
        var dxdy=getDxDyFromDirection(objectDirections[key])
        var neighbour=this.getTileObj(x+dxdy[0],y+dxdy[1])
        if (neighbour && neighbour.Type==objectTypes.ARROW){
            if (neighbour.Direction==objectDirections[key]){
                outputs=outputs+1
            }
            if (neighbour.Direction==getOppositeDirection(objectDirections[key])){
                inputs=inputs+1
            }
            if (this.resourceMap[x+dxdy[0]][y+dxdy[1]]!=resouceTypes.RESOURCE_NONE){
                nextToResource=true;
            }
        }
    }
    var dxdy=getDxDyFromDirection(self.Direction)
    var neighbour=this.getTileObj(x+dxdy[0],y+dxdy[1])
    var hasBlockAhead=false;
    if (neighbour && neighbour.Type!=objectTypes.EMPTY){
        hasBlockAhead=true;
    }

    if (this.resourceMap[x][y]!=resouceTypes.RESOURCE_NONE){
        return objectRoles.ROLE_MINE
    }
    if (inputs>1){
        return objectRoles.ROLE_FACTORY
    }
    if (hasBlockAhead || nextToResource){
        return objectRoles.ROLE_BELT
    }
    if (inputs==1){
      return objectRoles.ROLE_EXPORT  
    }
    return objectRoles.ROLE_STORAGE

}

function PlayerPlacingUI(board) {
    this.board = board
    this.ghostTile = game.add.sprite(0, 0, 'arrow');
    this.ghostTile.visible = false
    this.ghostTile.alpha = 0.4
    this.ghostTile.anchor.setTo(0.5, 0.5)
    var scale=board.tileSize/this.ghostTile.width
    this.ghostTile.scale.setTo(scale,scale)
    planningGroup.add(this.ghostTile)
    this.currentTileDirection = objectDirections.DIRECTION_UP
    this.currentLocation = undefined
}

PlayerPlacingUI.prototype.UpdateSpriteLocation = function() {
    if (this.currentLocation) {
        this.ghostTile.visible = true
        ret = this.board.getXYFromTile(this.currentLocation[0], this.currentLocation[1])
        this.ghostTile.x = ret[0]
        this.ghostTile.y = ret[1]
        if (playerPlacingUI.currentTileDirection == objectDirections.DIRECTION_UP) {
            this.ghostTile.angle = 0
        } else if (playerPlacingUI.currentTileDirection == objectDirections.DIRECTION_DOWN) {
            this.ghostTile.angle = 180
        } else if (playerPlacingUI.currentTileDirection == objectDirections.DIRECTION_LEFT) {
            this.ghostTile.angle = 270
        } else if (playerPlacingUI.currentTileDirection == objectDirections.DIRECTION_RIGHT) {
            this.ghostTile.angle = 90
        }
    } else {
        this.ghostTile.visible = false
    }

}

function keyPressed(e) {
    if (e.key == "ArrowUp" || e.key == "w") {
        playerPlacingUI.currentTileDirection = objectDirections.DIRECTION_UP
    } else if (e.key == "ArrowDown" || e.key == "s") {
        playerPlacingUI.currentTileDirection = objectDirections.DIRECTION_DOWN
    } else if (e.key == "ArrowLeft" || e.key == "a") {
        playerPlacingUI.currentTileDirection = objectDirections.DIRECTION_LEFT
    } else if (e.key == "ArrowRight" || e.key == "d") {
        playerPlacingUI.currentTileDirection = objectDirections.DIRECTION_RIGHT
    }
}

function preload() {
    game.load.image('arrow', 'images/arrow.png');
    game.load.image('Factory', 'images/F.png');
    game.load.image('Storage', 'images/S.png');
    game.load.image('Mine', 'images/M.png');
    game.load.image('Export', 'images/E.png');
    game.load.image('Unknown', 'images/U.png');
    game.load.image('iron', 'images/iron.png');
    game.load.image('coal', 'images/coal.png');
}

function create() {
    game.stage.backgroundColor = '#ffffff';

    renderingGroups = game.add.group()

    backgroundGroup = game.add.group()
    resourceGroup = game.add.group()
    factoryGroup = game.add.group()
    planningGroup = game.add.group()
    uiGroup = game.add.group()

    renderingGroups.add(backgroundGroup)
    renderingGroups.add(resourceGroup)
    renderingGroups.add(factoryGroup)
    renderingGroups.add(planningGroup)
    renderingGroups.add(uiGroup)

    gameBoard = new GameBoard()
    playerPlacingUI = new PlayerPlacingUI(gameBoard)

    this.game.input.keyboard.onDownCallback = keyPressed
    game.canvas.oncontextmenu = function(e) {
        e.preventDefault();
    }

    moneyText = game.add.text(0, 760, "Funds: "+Math.floor(player_money).toString() , style);
    uiGroup.add(moneyText);
}

function update() {
    playerPlacingUI.currentLocation = gameBoard.getTileFromXY(game.input.x, game.input.y)
    playerPlacingUI.UpdateSpriteLocation()

    if ((game.input.activePointer.leftButton.isDown) && playerPlacingUI.currentLocation && 
    	gameBoard.isTileFree(playerPlacingUI.currentLocation[0], playerPlacingUI.currentLocation[1])) {
        gameBoard.setTile(playerPlacingUI.currentLocation[0], playerPlacingUI.currentLocation[1], {
            Type: objectTypes.ARROW,
            Direction: playerPlacingUI.currentTileDirection
        })
    } else if ((game.input.activePointer.rightButton.isDown) && playerPlacingUI.currentLocation && 
    	!gameBoard.isTileFree(playerPlacingUI.currentLocation[0], playerPlacingUI.currentLocation[1])) {
        gameBoard.setTile(playerPlacingUI.currentLocation[0], playerPlacingUI.currentLocation[1], {
            Type: objectTypes.EMPTY,
            Direction: playerPlacingUI.currentTileDirection
        })
    }

    for (var i = 0; i < gameBoard.boardSize[0]; i++) {
        for (var j = 0; j < gameBoard.boardSize[1]; j++) {
            gameBoard.factoryMap[i][j].RunInternal()
        }
    }
    for (var i = 0; i < gameBoard.boardSize[0]; i++) {
        for (var j = 0; j < gameBoard.boardSize[1]; j++) {
            gameBoard.factoryMap[i][j].RunExternal()
        }
    }
    for (var i = 0; i < gameBoard.boardSize[0]; i++) {
        for (var j = 0; j < gameBoard.boardSize[1]; j++) {
            gameBoard.factoryMap[i][j].CurrentInventory=gameBoard.factoryMap[i][j].NextInventory
            for (var dirIndex=0;dirIndex<gameBoard.factoryMap[i][j].CurrentInventory.length;dirIndex++){
                if(gameBoard.factoryMap[i][j].CurrentInventory[dirIndex]==0){
                    gameBoard.factoryMap[i][j].ItemType[dirIndex]=itemTypes.ITEM_NONE
                }
            }
            gameBoard.factoryMap[i][j].NextInventory=[0,0,0,0,0]
        }
    }
}

function render() {
    for (var i = 0; i < gameBoard.boardSize[0]; i++) {
        for (var j = 0; j < gameBoard.boardSize[1]; j++) {
            if (gameBoard.factoryMap[i][j].Type==objectTypes.EMPTY) continue;
            if (gameBoard.factoryMap[i][j].CurrentInventory[gameBoard.factoryMap[i][j].Direction]){
                gameBoard.factorySpritesMap[i][j].tint=0x000000
            }else{
              var sum = gameBoard.factoryMap[i][j].CurrentInventory.reduce(function(pv, cv) { return pv + cv; }, 0);
                if (sum>0){
                    gameBoard.factorySpritesMap[i][j].tint=0x808080 
                }else{
                    gameBoard.factorySpritesMap[i][j].tint=0xffffff 
                }
            }
        }
    } 
    
    moneyText.text="Funds: "+Math.floor(player_money).toString()


}
